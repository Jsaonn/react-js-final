import React from "react";
import { Layout } from "antd";

const FooterComp = () => {

    const { Footer } = Layout;

    return (
        <Footer style={{ textAlign: "center", position: "relative", left: 0, bottom: 0, width: "100%" }}>
            Final Project ©2021 Created by Jsaon
        </Footer>
    )
}

export default FooterComp