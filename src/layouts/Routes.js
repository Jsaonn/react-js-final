import React, { useContext } from "react";
import {
    Switch,
    Route,
    Redirect
} from 'react-router-dom';
import Home from '../content/Home';
import MovieList from '../content/MovieList';
import MovieListDetail from '../content/MovieListDetail';
import GameList from '../content/GameList';
import GameListDetail from '../content/GameListDetails';
import { UserContext } from '../auth/UserContext';
import Register from '../auth/Register';
import Login from '../auth/Login';
import EditPassword from '../auth/EditPassword';
import MovieTable from '../edit/movie/MovieTable';
import MovieAddForm from '../edit/movie/MovieAddForm';
import MovieEditForm from '../edit/movie/MovieEditForm';
import GameTable from '../edit/game/GameTable';
import GameAddForm from '../edit/game/GameAddForm';
import GameEditForm from '../edit/game/GameEditForm';

const Routes = () => {

    const [user] = useContext(UserContext);

    const PrivateRoute = ({...props}) => {
        if (user) {
            return <Route {...props} />
        } else {
            return <Redirect to="/login" />
        }
    }

    const LoginRoute = ({...props}) =>
    user ? <Redirect to="/" /> : <Route {...props} />

    return (
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/movie-list" component={MovieList}/>
            <Route exact path="/movie-list/details/:idMovie" component={MovieListDetail}/>
            <Route exact path="/game-list" component={GameList}/>
            <Route exact path="/game-list/details/:idGame" component={GameListDetail}/>
            <LoginRoute exact path="/register" component={Register}/>
            <LoginRoute exact path="/login" component={Login}/>
            <PrivateRoute exact path="/edit-password" component={EditPassword}/>
            <PrivateRoute exact path="/table-movie" component={MovieTable}/>
            <PrivateRoute exact path="/table-movie/create-movie" component={MovieAddForm}/>
            <PrivateRoute exact path="/table-movie/edit-movie/:idMovie" component={MovieEditForm}/>
            <PrivateRoute exact path="/table-game" component={GameTable}/>
            <PrivateRoute exact path="/table-game/create-game" component={GameAddForm}/>
            <PrivateRoute exact path="/table-game/edit-game/:idGame" component={GameEditForm}/>
        </Switch>
    )
}

export default Routes