import React from "react";
import "antd/dist/antd.css";
import { Layout } from "antd";
import HeaderComp from './HeaderComp';
import FooterComp from './FooterComp';
import Routes from '../layouts/Routes';
import SidebarComp from './SidebarComp';

const Layouts = () => {

  const { Content } = Layout

    return(
      <Layout style={{ minHeight: '100vh' }}>
        <SidebarComp />
        <HeaderComp />
        <Layout className="site-layout" style={{marginTop: "4%"}}>
          <Content>
            <Routes />
          </Content>
          <FooterComp />
        </Layout>
      </Layout>
    )
}

export default Layouts
