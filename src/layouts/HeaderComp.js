import React, { useContext } from "react";
import "antd/dist/antd.css";
import { Layout, Menu } from "antd";
import { Link } from "react-router-dom";
import { UserContext } from "../auth/UserContext";

const HeaderComp = () => {
    const { Header } = Layout;
    const { SubMenu } = Menu;
    const [user, setUser] = useContext(UserContext)

    const handleLogout = () => {
      console.log(user.name);
      setUser(null)
      localStorage.removeItem("user")
    }

    return (
      <Header style={{ position: "fixed", zIndex: 1, width: "100%"}}>
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" style={{marginLeft: "70%"}}>
          <Menu.Item key="1"><Link to="/">Home</Link></Menu.Item>
          <Menu.Item key="2"><Link to="/movie-list">Movies</Link></Menu.Item>
          <Menu.Item key="3"><Link to="/game-list">Games</Link></Menu.Item>
          { user ? 
            <>
              <SubMenu key="4" title={`Welcome, ${user.name}`}>
                <Menu.Item key="4-1"><Link to="/edit-password">Change Pasword</Link></Menu.Item>
                <Menu.Item key="4-2"><a style={{cursor: "pointer"}} onClick={handleLogout}>Logout </a></Menu.Item>
              </SubMenu>
            </>
            :
            <>
            <Menu.Item key="5"><Link to="/login">Login</Link></Menu.Item>
            </>
        }
        </Menu>
      </Header>
    )
}

export default HeaderComp