import React, { useEffect, useContext } from "react"
import axios from "axios"
import { useHistory } from "react-router-dom"
import { UserContext } from "../../auth/UserContext"
import { Layout, PageHeader, Form, Input, Button, Select, InputNumber, message, Slider } from "antd";

const { Content } = Layout;
const { Option } = Select;
const { TextArea } = Input;

const MovieAddForm = () => {
    let history = useHistory()
    const [user] = useContext(UserContext)
    const [form] = Form.useForm()

    useEffect(() => {
        form.setFieldsValue({
            duration: 100,
            year: 2021,
        })
    })

    const onFinish = (values) => {
        console.log("submitted");
        let header = user.token
        console.log(values);
        axios.post('https://backendexample.sanbersy.com/api/data-movie',
            {title: values.title, description: values.description, year: values.year, duration: values.duration, genre: values.genre.toString(), rating: values.rating, review: values.review, image_url: values.image_url}
            , {headers: { Authorization: `Bearer ${header}`}
        }).then((res) => {
            history.push('/table-movie')
            message.success("Movie successfully added")
        })
    }

    return (
        <Content style={{marginLeft: "30%", marginTop: "3%", width: "50%"}}>
            <PageHeader
                className="site-page-header"
                onBack={() => history.push("/table-movie")}
                title="Add New Movie"
            />
            <div className="boxed">
                <Form
                    form={form}
                    name="basic"
                    onFinish={onFinish}
                    labelCol={{ span: 4 }}
                    wrapperCol={{ span: 16 }}
                >
                    <Form.Item
                        label="Movie Title"
                        name="title"
                        rules={[
                            {
                                required: true,
                                message: "Please input Movie Title!",
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                        
                    <Form.Item
                        label="Description"
                        name="description"
                        rules={[
                            {
                                required: true,
                                message: "Please input Movie Description!",
                            },
                        ]}
                    >
                        <TextArea autoSize />
                    </Form.Item>

                    <Form.Item
                        label="Release Year"
                        name="year"
                        rules={[
                            {
                                required: true,
                                message: "Please input Release Year!",
                            },
                        ]}
                    >
                        <InputNumber min={1980} max={2021} />
                    </Form.Item>

                    <Form.Item
                        label="Duration (Minutes)"
                        name="duration"
                        rules={[
                            {
                                required: true,
                                message: "Please input Movie Duration!",
                            },
                        ]}
                    >
                        <InputNumber min={0} />
                    </Form.Item>

                    <Form.Item
                        label="Genre"
                        name="genre"
                        rules={[
                            {
                                required: true,
                                message: "Please input Genres of the movie!",
                            },
                        ]}
                    >
                        <Select
                            mode="multiple"
                            allowClear
                            style={{ width: "100%" }}
                            placeholder="Please select"
                        >
                            <Option key="1" value="Action">
                                Action
                            </Option>
                            <Option key="2" value="Adventure">
                                Adventure
                            </Option>
                            <Option key="3" value="Animation">
                                Animation
                            </Option>
                            <Option key="4" value="Comedy">
                                Comedy
                            </Option>
                            <Option key="5" value="Crime">
                                Crime
                            </Option>
                            <Option key="6" value="Documentary">
                                Documentary
                            </Option>
                            <Option key="7" value="Drama">
                                Drama
                            </Option>
                            <Option key="8" value="Family">
                                Family
                            </Option>
                            <Option key="9" value="Fantasy">
                                Fantasy
                            </Option>
                            <Option key="10" value="History">
                                History
                            </Option>
                            <Option key="11" value="Horror">
                                Horror
                            </Option>
                            <Option key="12" value="Music">
                                Music
                            </Option>
                            <Option key="13" value="Mystery">
                                Mystery
                            </Option>
                            <Option key="14" value="Romance">
                                Romance
                            </Option>
                            <Option key="15" value="Science Fiction">
                                Science Fiction
                            </Option>
                            <Option key="16" value="TV Movie">
                                TV Movie
                            </Option>
                            <Option key="17" value="Thriller">
                                Thriller
                            </Option>
                            <Option key="18" value="War">
                                War
                            </Option>
                            <Option key="19" value="Western">
                                Western
                            </Option>
                        </Select>
                    </Form.Item>

                    <Form.Item
                        label="Rating"
                        name="rating"
                        rules={[
                            {
                                required: true,
                                message: "Please input Movie Rating!",
                            },
                        ]}
                    >
                        <Slider min={0} max={10} />
                    </Form.Item>

                    <Form.Item
                        label="Review"
                        name="review"
                        rules={[
                            {
                                required: true,
                                message: "Please input Movie Review!",
                            },
                        ]}
                    >
                        <TextArea autoSize />
                    </Form.Item>

                    <Form.Item
                        label="Image URL"
                        name="image_url"
                        rules={[
                            {
                                required: true,
                                message: "Please input Url Image!",
                            },
                            {
                                type: "url",
                                message: "The input is not valid Url Image!",
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit">
                            Add
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </Content>
    )
}

export default MovieAddForm