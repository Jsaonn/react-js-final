import React, {useState, useEffect, useContext} from "react"
import axios from "axios"
import { Link } from "react-router-dom"
import { UserContext } from "../../auth/UserContext"
import { Layout, Table, message, Typography, Button, Image, Input, Popconfirm } from "antd";

const { Search } = Input;
const { Title } = Typography;
const { Content } = Layout;

const MovieTable = () => {
    const [data, setData] = useState([])
    const [search, setSearch] = useState(null)
    const [user] = useContext(UserContext)
    const [trigger, setTrigger] = useState(true)

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie`)

            setData(
                result.data.map(el => {
                    return {
                        id: el.id,
                        title: el.title,
                        description: el.description,
                        year: el.year,
                        duration: el.duration,
                        genre: el.genre,
                        rating: el.rating,
                        review: el.review,
                        image_url: el.image_url
                    }
                })
            )
        }
        if(trigger) {
            fetchData()
            console.log("get");
            setTrigger(false)
        }
    },[trigger])

    const columns = [
        {
            title: "No",
            dataIndex: "no",
            sorter: {
                compare: (a, b) => a.no - b.no,
            },
        },
        {
            title: "Image",
            dataIndex: "image_url",
        },
        {
            title: "Title",
            dataIndex: "title",
            sorter: {
                compare: (a, b) => {
                    if (a.title < b.title) return -1;
                    if (b.title < a.title) return 1;
                    return 0;
                },
            },
        },
        {
            title: "Description",
            dataIndex: "description",
            sorter: {
                compare: (a, b) => {
                    if (a.description < b.description) return -1;
                    if (b.description < a.description) return 1;
                    return 0;
                },
            },
        },
        {
            title: "Year",
            dataIndex: "year",
            sorter: {
                compare: (a, b) => a.year - b.year,
            },
        },
        {
            title: "Duration",
            dataIndex: "duration",
            sorter: {
                compare: (a, b) => a.duration - b.duration,
            },
        },
        {
            title: "Genre",
            dataIndex: "genre",
            sorter: {
                compare: (a, b) => {
                    if (a.genre < b.genre) return -1;
                    if (b.genre < a.genre) return 1;
                    return 0;
                },
            },
        },
        {
            title: "Rating",
            dataIndex: "rating",
            sorter: {
                compare: (a, b) => a.rating - b.rating,
            },
        },
        {
            title: "Review",
            dataIndex: "review",
            sorter: {
                compare: (a, b) => {
                    if (a.review < b.review) return -1;
                    if (b.review < a.review) return 1;
                    return 0;
                },
            }
        },
        {
            title: "Action",
            dataIndex: "action",
        }
    ]

    const handleDelete = (values) => {
        let header = user.token
        console.log("deleted");
        axios.delete(`https://backendexample.sanbersy.com/api/data-movie/${values}`, {
            headers: { Authorization: `Bearer ${header}`}
        })
        .then( () => {
            message.success("Movie deleted!");
            setTrigger(true)
        })
    }

    const handleChange = (event) => {
        setSearch(event.target.value);
    }

    const handleSearch = () => {
        if (search) {
            let value = data.filter((el) => {
                if(el) {
                    return el.title.toLowerCase().includes(search);
                }
            });
            setData(value)
        } else {
            axios.get(`https://backendexample.sanbersy.com/api/data-movie`).then((res) => {
                setData(res.data)
            });
        }
        setSearch("");
    }

    const datas =
        data !== null &&
        data.map((el, key) => {
            return {
                key: key,
                no: key + 1,
                image_url:
                    <Image
                        width={100}
                        src={el.image_url}
                        style={{ objectFit: "cover" }}
                    />,
                title: el.title,
                description:
                    el.description !== null
                        ? el.description.length > 20
                            ? el.description.substr(0, 20) + "..."
                            : el.description
                        : null,
                year: el.year,
                duration: el.duration,
                genre: el.genre,
                rating: el.rating,
                review: 
                    el.review !== null
                        ? el.review.length > 20
                            ? el.review.substr(0, 20) + "..."
                            : el.review
                        : null,
                action: (
                    <>
                        <Button
                            type="primary"
                            size="small"
                            style={{marginBottom: "10%"}}
                        >
                            <Link to={`/table-movie/edit-movie/${el.id}`}>Edit</Link>
                        </Button><br />
                        <Popconfirm
                            placement="topRight"
                            title={`Are you sure want to delete ${el.title}?`}
                            onConfirm={() => {
                                handleDelete(el.id);
                            }}
                            okText="Yes"
                            cancelText="No"
                        >
                            <Button
                                type="primary"
                                danger
                                size="small"
                            >
                                Delete
                            </Button>
                            
                        </Popconfirm>
                    </>
                ),
            }
        })
    
        return (
            <>
                <Content style={{ minHeight: "100vh", marginBottom: "5%" }}>
                    <section style={{ padding: 24 }}>
                        <Title style={{ margin: "20px" }}>
                            Movies
                            <Button
                                type="primary"
                                shape="round"
                                style={{ float: "right" }}
                            >
                                <Link
                                    to="/table-movie/create-movie"
                                    style={{ color: "#ffffff" }}
                                >
                                    Add
                                </Link>
                            </Button>
                        </Title>
                        <Search
                            onChange={handleChange}
                            value={search}
                            placeholder="input search by Name"
                            onSearch={() => handleSearch()}
                            style={{ width: 200, float: "left" }}
                        />
                        <Table columns={columns} dataSource={datas} />
                    </section>
                </Content>
            </>
        )
}

export default MovieTable