import React, {useState, useEffect, useContext} from "react"
import axios from "axios"
import { Link } from "react-router-dom"
import { UserContext } from "../../auth/UserContext"
import { Layout, Table, message, Typography, Button, Image, Input, Popconfirm } from "antd";
import { CheckCircleTwoTone } from "@ant-design/icons";

const { Search } = Input;
const { Title } = Typography;
const { Content } = Layout;

const GameTable = () => {
    const [data, setData] = useState([])
    const [search, setSearch] = useState(null)
    const [user] = useContext(UserContext)
    const [trigger, setTrigger] = useState(true)

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.get(`https://backendexample.sanbersy.com/api/data-game`)

            setData(
                result.data.map(el => {
                    return {
                        id: el.id,
                        name: el.name,
                        genre: el.genre,
                        singlePlayer: el.singlePlayer,
                        multiplayer: el.multiplayer,
                        platform: el.platform,
                        release: el.release,
                        image_url: el.image_url
                    }
                })
            )
        }
        if(trigger) {
            fetchData()
            console.log("get");
            setTrigger(false)
        }
    },[trigger])

    const columns = [
        {
            title: "No",
            dataIndex: "no",
            sorter: {
                compare: (a, b) => a.no - b.no,
            },
        },
        {
            title: "Image",
            dataIndex: "image_url",
        },
        {
            title: "Name",
            dataIndex: "name",
            sorter: {
                compare: (a, b) => {
                    if (a.name < b.name) return -1;
                    if (b.name < a.name) return 1;
                    return 0;
                },
            },
        },
        {
            title: "Genre",
            dataIndex: "genre",
            sorter: {
                compare: (a, b) => {
                    if (a.genre < b.genre) return -1;
                    if (b.genre < a.genre) return 1;
                    return 0;
                },
            },
        },
        {
            title: "SinglePlayer",
            dataIndex: "singlePlayer",
        },
        {
            title: "MultiPlayer",
            dataIndex: "multiplayer",
        },
        {
            title: "Platform",
            dataIndex: "platform",
            sorter: {
                compare: (a, b) => {
                    if (a.platform < b.platform) return -1;
                    if (b.platform < a.platform) return 1;
                    return 0;
                },
            }
        },
        {
            title: "Release",
            dataIndex: "release",
            sorter: {
                compare: (a, b) => a.release - b.release,
            },
        },
        {
            title: "Action",
            dataIndex: "action",
        }
    ]

    const handleDelete = (values) => {
        let header = user.token
        axios.delete(`https://backendexample.sanbersy.com/api/data-game/${values}`, {
            headers: { Authorization: `Bearer ${header}`}
        })
        .then( () => {
            message.success("Game deleted!");
            setTrigger(true)
        })
    }

    const handleChange = (event) => {
        setSearch(event.target.value);
    }

    const handleSearch = () => {
        if (search) {
            let value = data.filter((el) => {
                if(el) {
                    return el.name.toLowerCase().includes(search);
                }
            });
            setData(value)
        } else {
            axios.get(`https://backendexample.sanbersy.com/api/data-game`).then((res) => {
                setData(res.data)
            });
        }
        setSearch("");
    }

    const datas =
        data !== null &&
        data.map((el, key) => {
            return {
                key: key,
                no: key + 1,
                image_url:
                    <Image
                        width={100}
                        src={el.image_url}
                        style={{ objectFit: "cover" }}
                    />,
                name: el.name,
                genre: el.genre,
                singlePlayer:
                el.singlePlayer === 1 ? (
                    <CheckCircleTwoTone twoToneColor="#52c41a" />
                    ) : (
                        <CheckCircleTwoTone twoToneColor="#dddddd" />
                        ),
                        multiplayer:
                        el.multiplayer === 1 ? (
                            <CheckCircleTwoTone twoToneColor="#52c41a" />
                    ) : (
                        <CheckCircleTwoTone twoToneColor="#dddddd" />
                        ),
                release: el.release,
                platform: el.platform,
                action: (
                    <>
                        <Button
                            type="primary"
                            size="small"
                            style={{marginBottom: "10%"}}
                        >
                            <Link to={`/table-game/edit-game/${el.id}`}>Edit</Link>
                        </Button><br />
                        <Popconfirm
                            placement="topRight"
                            title={`Are you sure want to delete ${el.name}?`}
                            onConfirm={() => {
                                handleDelete(el.id);
                            }}
                            okText="Yes"
                            cancelText="No"
                        >
                            <Button
                                type="primary"
                                danger
                                size="small"
                            >
                                Delete
                            </Button>
                            
                        </Popconfirm>
                    </>
                ),
            }
        })
    
        return (
            <>
                <Content style={{ minHeight: "100vh", marginBottom: "5%" }}>
                    <section style={{ padding: 24 }}>
                        <Title style={{ margin: "20px" }}>
                            Games
                            <Button
                                type="primary"
                                shape="round"
                                style={{ float: "right" }}
                            >
                                <Link
                                    to="/table-game/create-game"
                                    style={{ color: "#ffffff" }}
                                >
                                    Add
                                </Link>
                            </Button>
                        </Title>
                        <Search
                            onChange={handleChange}
                            value={search}
                            placeholder="input search by Name"
                            onSearch={() => handleSearch()}
                            style={{ width: 200, float: "left" }}
                        />
                        <Table columns={columns} dataSource={datas} />
                    </section>
                </Content>
            </>
        )
}

export default GameTable