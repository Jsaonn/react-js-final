import './App.css';
import Layouts from './layouts/Layouts';
import { BrowserRouter as Router } from "react-router-dom";
import { UserProvider } from "../src/auth/UserContext";

function App() {
  return (
    <div className="App">
      <UserProvider>
        <Router>
          <Layouts />
        </Router>
      </UserProvider>
    </div>
  );
}

export default App;
