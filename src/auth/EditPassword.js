import React, { useContext } from "react";
import { UserContext } from "./UserContext";
import { Layout, message, Form, Input, Button, Typography } from "antd";
import { useHistory } from "react-router-dom";
import axios from "axios";
const { Content } = Layout;
const { Title } = Typography;

const formItemLayout = {
    labelCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 8,
      },
    },
    wrapperCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 16,
      },
    },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

const EditPassword = () => {
    let history = useHistory();
    const [user, setUser] = useContext(UserContext);
    const [form] = Form.useForm();

    const onFinish = (values) => {
        let header = user.token;
        axios.post(`https://backendexample.sanbersy.com/api/change-password`,
        {current_password: values.old_password, new_password: values.new_password, new_confirm_password: values.confirm}
        ,{
            headers: { Authorization: `Bearer ${header}` },
        })
            .then((res) => {
                message.success("Password has been changed");
                history.push("/login");
            })
            .catch((error) => {
                message.error(error.response.data.error);
                if (error.response.status === 400) {
                    setUser(null);
                    localStorage.clear();
                }
            })
        }

    return (
        <Content>
            <section>
                <div>
                    <Title style={{ textAlign: "center", marginTop: "10%" }}>
                        Edit Password
                    </Title>
                    <Form
                        {...formItemLayout}
                        form={form}
                        name="change-password"
                        onFinish={onFinish}
                        scrollToFirstError
                        style={{width: "40%", marginLeft: "25%", marginTop: "2%"}}
                    >
                        <Form.Item
                            name="old_password"
                            label="Old Password"
                            rules={[
                                {
                                    required: true,
                                    message: "Please input your password!",
                                },
                                {
                                    min: 6,
                                    message:
                                        "Password must be minimum 6 characters.",
                                },
                            ]}
                            hasFeedback
                        >
                            <Input.Password />
                        </Form.Item>
                        <Form.Item
                            name="new_password"
                            label="New Password"
                            rules={[
                                {
                                    required: true,
                                    message: "Please input your password!",
                                },
                                {
                                    min: 6,
                                    message:
                                        "Password must be minimum 6 characters.",
                                },
                            ]}
                            hasFeedback
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item
                            name="confirm"
                            label="Confirm Password"
                            dependencies={["password"]}
                            hasFeedback
                            rules={[
                                {
                                    required: true,
                                    message: "Please confirm your password!",
                                },
                                ({ getFieldValue }) => ({
                                    validator(rule, value) {
                                        if (
                                            !value ||
                                            getFieldValue("new_password") ===
                                                value
                                        ) {
                                            return Promise.resolve();
                                        }
                                        return Promise.reject(
                                            "The two passwords that you entered do not match!",
                                        );
                                    },
                                }),
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button type="primary" htmlType="submit">
                                Edit
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </section>
        </Content>
    );
}

export default EditPassword