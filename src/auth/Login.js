import React, { useContext } from "react"
import { UserContext } from "./UserContext"
import axios from "axios"
import { Link, useHistory } from "react-router-dom"
import 'antd/dist/antd.css';
import { Form, Input, Button, message } from 'antd';
import { MailOutlined, LockOutlined } from '@ant-design/icons';

const Login = () =>{
    const [, setUser] = useContext(UserContext)
    let history = useHistory()
  
    const onFinish = (values) =>{
      axios.post("https://backendexample.sanbersy.com/api/user-login", {
        email: values.email, 
        password: values.password
      }).then(
        (res)=>{
          var user = res.data.user
          var token = res.data.token
          var currentUser = {name: user.name, email: user.email, token }
          setUser(currentUser)
          localStorage.setItem("user", JSON.stringify(currentUser))
          message.success("You successfully log on!");
          history.push('/')
          console.log("login");
        }
      ).catch((err)=>{
        alert(JSON.stringify(err.response.data))
      })
    }
  
    return (
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        style={{marginLeft: "38%", marginTop: "10%", width: "30%"}}
      >
        <Form.Item
          name="email"
          rules={[
            {
              required: true,
              message: 'Please input your Email!',
            },
          ]}
        >
          <Input prefix={<MailOutlined className="site-form-item-icon" />} placeholder="Email" />
        </Form.Item>

        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: 'Please input your Password!',
            },
          ]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Password"
          />
        </Form.Item>
  
        <Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button">
            Log in
          </Button><br />
          Or <Link to="/register">register now!</Link>
        </Form.Item>
      </Form>
    );
}

export default Login