import React, { useContext } from "react"
import { UserContext } from "./UserContext"
import axios from "axios"
import 'antd/dist/antd.css';
import { Form, Input, Button, message } from 'antd';
import { useHistory, Link } from "react-router-dom" 

const formItemLayout = {
    labelCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 8,
      },
    },
    wrapperCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 16,
      },
    },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

const Register = () => {
    const [form] = Form.useForm();
    const [, setUser] = useContext(UserContext)
    let history = useHistory()

  const onFinish = (values) => {
    axios.post(`https://backendexample.sanbersy.com/api/register`, {
        name: values.name,
        email: values.email,
        password: values.password
        }).then(
            (res) => {
                var user = res.data.user
                var token = res.data.token
                var currentUser = {name: user.name, email: user.email, token}
                setUser(currentUser)
                localStorage.setItem("user", JSON.stringify(currentUser))
                history.push('/login')
                message.success("Your account is ready!");
                console.log("register");
            }
        ).catch((err) => {
            alert(JSON.stringify(err.response.data))
        })
    }

  return (
    <Form
      {...formItemLayout}
      form={form}
      name="register"
      onFinish={onFinish}
      scrollToFirstError
      style={{marginLeft: "35%", marginTop: "10%", width: "30%"}}
    >
      <Form.Item
        name="email"
        label="E-mail"
        rules={[
          {
            type: 'email',
            message: 'The input is not valid E-mail!',
          },
          {
            required: true,
            message: 'Please input your E-mail!',
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        name="password"
        label="Password"
        rules={[
          {
            required: true,
            message: 'Please input your password!',
          },
        ]}
        hasFeedback
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        name="confirm"
        label="Confirm Password"
        dependencies={['password']}
        hasFeedback
        rules={[
          {
            required: true,
            message: 'Please confirm your password!',
          },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue('password') === value) {
                return Promise.resolve();
              }

              return Promise.reject(new Error('The two passwords that you entered do not match!'));
            },
          }),
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        name="name"
        label="Name"
        tooltip="What do you want others to call you?"
        rules={[
          {
            required: true,
            message: 'Please input your nickname!',
            whitespace: true,
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item {...tailFormItemLayout}>
        <Button type="primary" htmlType="submit">
          Register
        </Button><br />
        <Link to="/login">I already had an account</Link>
      </Form.Item>
    </Form>
  );
}

export default Register