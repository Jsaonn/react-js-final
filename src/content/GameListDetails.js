import React from "react"
import { Link } from "react-router-dom";
import axios from "axios"
import { Col, Row, Image } from 'antd';

class GameListDetail extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            id: this.props.match.params.idGame
        }
    }

    componentDidMount() {
        axios.get(`https://backendexample.sanbersy.com/api/data-game/${this.state.id}`)
        .then(res => this.setState(
            {
                data: res.data
            }
        ))
    }

    render() {
        const { data } = this.state
        return (
            <>
                <div className="game-detail">
                    <Row>
                        <Col span={8} offset={1}>
                            <Image width= "70%"
                            src={data.image_url} />
                        </Col>
                        <Col span={12} offset={1}>
                            <h1>{data.name} ({data.release})</h1>
                            <hr />
                            <strong> Genre: </strong>
                            <p>{data.genre}</p>
                            <hr />
                            <strong> Platform: </strong>
                            <p>{data.platform}</p>
                            <hr />
                            <strong> Mode: </strong>
                            <p>
                                {data.singlePlayer > 0 && (
                                    <div>
                                        Single Player
                                    </div>
                                )}
                                {data.multiplayer > 0 && (
                                    <div>
                                        Multi Player
                                    </div>
                                )}
                            </p>
                        </Col>
                    </Row>
                </div>
            </>
        )
    }
}

export default GameListDetail