import React, {useState, useEffect} from "react"
import axios from "axios"
import { Link } from "react-router-dom"
import 'antd/dist/antd.css'
import { Card, Col, Row } from 'antd';

const GameList = () => {
    const [data, setData] = useState([]);
    const { Meta } = Card;

    useEffect( () => {
        const fetchData = async ()=> {
            const result = await axios.get(`https://backendexample.sanbersy.com/api/data-game`)
            setData(result.data.map(el => {
                return {
                    id: el.id,
                    name: el.name,
                    genre: el.genre,
                    singlePlayer: el.singlePlayer,
                    multiPlayer: el.multiPlayer,
                    platform: el.platform,
                    release: el.release,
                    image_url: el.image_url
                }
            }))
        }
        fetchData();
    }, [])
    
    return(
        <div className="container-movie">
            <div className="site-card-wrapper">
                <Row gutter={16}>
            {
                data.map((val) => {
                    return(
                        <Col span={5}>
                            <Link to={`/game-list/details/${val.id}`}>
                                <Card
                                    hoverable
                                    style={{ width: 240, height: 450, marginLeft: "50%", marginBottom: "10%", borderTopLeftRadius: "10%", borderTopRightRadius: "10%" }}
                                    cover={<img src={val.image_url} style={{ height: 358.6, borderRadius: "10%", objectFit: "cover" }} />}
                                    bordered={false}
                                    >
                                    <Meta title={val.name} description={val.release}></Meta>
                                </Card>
                            </Link>
                        </Col>
                    )
                })
            }
                </Row>
            </div>
        </div>
    )
}

export default GameList