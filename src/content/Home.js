import React, { useEffect, useState } from "react";
import { Carousel, Layout, Typography, Card, Col, Row, Empty, Divider, Progress, message } from "antd"
import { Link } from "react-router-dom";
import axios from "axios";

const { Meta } = Card;
const { Title } = Typography;
const { Content } = Layout;

const Home = () => {
    const [movie, setMovie] = useState([])
    const [game, setGame] = useState([])

    useEffect(() => {
        axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        .then((res) => {
            setMovie(res.data)
        })
        axios.get(`https://backendexample.sanbersy.com/api/data-game`)
        .then((res)=> {
            setGame(res.data)
        })
    })

    const contentStyle = {
        height: '150px',
        color: '#fff',
        lineHeight: '160px',
        textAlign: 'center',
        background: '#364d79',
      };

    return (
        <div className="site-layout-background" style={{ padding: 24, textAlign: 'center' }}>
            <Carousel autoplay style={{ padding: "0px" }}>
                <div>
                    <h1 style={contentStyle}>
                        We have a lot of movies
                    </h1>
                    </div>
                    <div>
                    <h1 style={contentStyle}>We also have a lot of games</h1>
                </div>
            </Carousel>
            <Content>
                <section style={{ padding: 24 }}>
                    <Title level={3} style={{ margin: "0px 0px 20px 0px" }}>
                        Popular Movies
                    </Title>
                    <div className="site-card-wrapper">
                        <Row gutter={16}>
                            {movie !== null &&
                                movie.slice(0, 4).map((daftar, key) => {
                                    return (
                                        <Col span={6} key={key}>
                                            <Link
                                                to={`/movies/${daftar.id}/detail`}
                                            >
                                                <Card
                                                    hoverable
                                                    style={{
                                                        width: 240,
                                                        border: "0px",
                                                        position:
                                                            "relative",
                                                    }}
                                                    cover={
                                                        daftar.image_url ? (
                                                            <img
                                                                alt={
                                                                    daftar.title
                                                                }
                                                                style={{
                                                                    borderRadius:
                                                                        "10px",
                                                                    height:
                                                                        "358.6px",
                                                                    objectFit:
                                                                        "cover",
                                                                }}
                                                                src={
                                                                    daftar.image_url
                                                                }
                                                            />
                                                        ) : (
                                                            <Empty
                                                                style={{
                                                                    borderRadius:
                                                                        "10px",
                                                                    height:
                                                                        "358.6px",
                                                                    objectFit:
                                                                        "cover",
                                                                }}
                                                            />
                                                        )
                                                    }
                                                >
                                                    <Meta
                                                        title={
                                                            <span
                                                                style={{
                                                                    fontWeight:
                                                                        "bold",
                                                                }}
                                                            >
                                                                {
                                                                    daftar.title
                                                                }
                                                            </span>
                                                        }
                                                        description={
                                                            daftar.year
                                                        }
                                                    />
                                                </Card>
                                            </Link>
                                        </Col>
                                    );
                                })}
                        </Row>
                    </div>
                    <Divider />
                    <Title level={3} style={{ margin: "0px 0px 20px 0px" }}>
                        Popular Games
                    </Title>
                    <div className="site-card-wrapper">
                        <Row gutter={16}>
                            {game !== null &&
                                game.slice(0, 4).map((daftar, key) => {
                                    return (
                                        <Col span={6} key={key}>
                                            <Link
                                                to={`/games/${daftar.id}/detail`}
                                            >
                                                <Card
                                                    hoverable
                                                    style={{
                                                        width: 240,
                                                        border: "0px",
                                                        position:
                                                            "relative",
                                                    }}
                                                    cover={
                                                        daftar.image_url ? (
                                                            <img
                                                                alt={
                                                                    daftar.title
                                                                }
                                                                style={{
                                                                    borderRadius:
                                                                        "10px",
                                                                    height:
                                                                        "358.6px",
                                                                    objectFit:
                                                                        "cover",
                                                                }}
                                                                src={
                                                                    daftar.image_url
                                                                }
                                                            />
                                                        ) : (
                                                            <Empty
                                                                style={{
                                                                    borderRadius:
                                                                        "10px",
                                                                    height:
                                                                        "358.6px",
                                                                    objectFit:
                                                                        "cover",
                                                                }}
                                                            />
                                                        )
                                                    }
                                                >
                                                    <Meta
                                                        title={
                                                            <span
                                                                style={{
                                                                    fontWeight:
                                                                        "bold",
                                                                }}
                                                            >
                                                                {
                                                                    daftar.name
                                                                }
                                                            </span>
                                                        }
                                                        description={
                                                            <span>
                                                                {
                                                                    daftar.release
                                                                }
                                                            </span>
                                                        }
                                                    />
                                                </Card>
                                            </Link>
                                        </Col>
                                    );
                                })}
                        </Row>
                    </div>
                  </section>
                </Content>
        </div>
    )
}

export default Home