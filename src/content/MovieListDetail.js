import React from "react"
import axios from "axios"
import { Col, Row, Image, Rate } from 'antd';

class MovieListDetail extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            id: this.props.match.params.idMovie,
        }
    }

    componentDidMount() {
        axios.get(`https://backendexample.sanbersy.com/api/data-movie/${this.state.id}`)
        .then(res => this.setState(
            {
                data: res.data
            }
        ))
    }

    render() {
        const { data } = this.state
        console.log(data);
        return (
            <>
                <div className="movie-detail">
                    <Row>
                        <Col span={8} offset={1}>
                            <Image width= "70%"
                            src={data.image_url} />
                        </Col>
                        <Col span={12} offset={1}>
                            <h1>{data.title} ({data.year})</h1>
                            <hr />
                            <p>{data.genre} | {data.duration} minutes</p>
                            <hr />
                            <strong> Description: </strong>
                            <p>{data.description}</p>
                            <hr />
                            <strong> Rating: </strong>
                            <Rate allowHalf disabled value={data.rating/2} />
                        </Col>
                    </Row>
                </div>
            </>
        )
    }
}

export default MovieListDetail