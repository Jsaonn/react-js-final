import React, {useState, useEffect} from "react"
import axios from "axios"
import { Link } from "react-router-dom"
import 'antd/dist/antd.css'
import { Card, Col, Row, Rate } from 'antd';

const MovieList = () => {
    const [data, setData] = useState([]);
    const { Meta } = Card;

    useEffect( () => {
        const fetchData = async ()=> {
            const result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
            setData(result.data.map(el => {
                return {
                    id: el.id,
                    title: el.title,
                    description: el.description,
                    year: el.year,
                    duration: el.duration,
                    genre: el.genre,
                    rating: el.rating,
                    review: el.review,
                    image_url: el.image_url
                }
            }))
        }
        fetchData();
    }, [])
    
    return(
        <div className="container-movie">
            <div className="site-card-wrapper">
                <Row gutter={16}>
            {
                data.map((val) => {
                    return(
                        <Col span={5}>
                            <Link to={`/movie-list/details/${val.id}`}>
                                <Card
                                    hoverable
                                    style={{ width: 240, height: 450, marginLeft: "50%", marginBottom: "10%", borderTopLeftRadius: "10%", borderTopRightRadius: "10%" }}
                                    cover={<img src={val.image_url} style={{ height: 358.6, borderRadius: "10%", objectFit: "cover" }} />}
                                    bordered={false}
                                    >
                                    <Meta title={val.title} description={<Rate allowHalf disabled defaultValue={val.rating/2} />}></Meta>
                                </Card>
                            </Link>
                        </Col>
                    )
                })
            }
                </Row>
            </div>
        </div>
    )
}

export default MovieList